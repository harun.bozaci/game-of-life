#ifndef OBSERVABLE_H
#define OBSERVABLE_H
#include <vector>
#include <algorithm>

template<typename> class Observer;

template <typename T>
class Observable {
public:
	void state_changed(T& subject) {
		for(auto obs : m_vobs) {
			obs->state_changed(subject);
		}
	}

	void subscribe(Observer<T>& obs) {
		m_vobs.push_back(&obs);
	}

	void unsubscribe(Observer<T>& obs) {
		// ADL
		m_vobs.erase(remove(begin(m_vobs), end(m_vobs), &obs), end(m_vobs));
		// C++20 std::erase can also be used instead of using Erase-Remove Idiom ( as seen above)
		// erase(begin(m_vobs), end(m_vobs), &obs);
	}
private:
	std::vector<Observer<T>*> m_vobs;
};

#endif

