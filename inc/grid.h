#ifndef GRID_H
#define GRID_H

#include "cell.h"
#include "cell_observer.h"
#include <array>
#include <cstdint>

template <std::size_t Row
		, std::size_t Col
		, template<std::size_t, std::size_t>typename OutputPolicy>
class Grid {
public:
	Grid() {
		for(std::size_t row = 0; row < Row; ++row) {
			for(std::size_t col = 0; col < Col; ++col) {
				discover_neighbours(static_cast<int>(row), static_cast<int>(col));
			}
		}
	}
	void make_cell_alive(std::size_t row, std::size_t col) {
		m_cells[row][col].set_next_state(Cell::State::ALIVE);
		m_cells[row][col].update_state();
	}
	void iterate() {
		for(std::size_t row = 0; row < Row; ++row) {
			for(std::size_t col = 0; col < Col; ++col) {
				auto count = m_cell_observers[row][col].get_alive_neightbour_count();
				switch(m_cells[row][col].get_state()) {
					using enum Cell::State;
					case ALIVE:
							if(count < 2 || count > 3)
								m_cells[row][col].set_next_state(DEAD);
						break;
					case DEAD:
							if(count == 3)
								m_cells[row][col].set_next_state(ALIVE);
						break;
				}
			}
		}
		for(auto& row : m_cells)
			for(auto& cell : row)
				cell.update_state();
	}

	void display() const {
		m_op.print(m_cells);
	}
private:
	void discover_neighbours(int row, int col) {
		int i_Row = static_cast<int>(Row);
		int i_Col = static_cast<int>(Col);
		for(int i = -1; i < 2; ++i) {
			auto r = row + i;
			if(0 > r || r >= i_Row)
				continue;
			for(int j = -1; j < 2; ++j) {
				auto c = col + j;
				if((i == 0 && j == 0) || 0 > c || c >= i_Col)
					continue;
				m_cells[row][col].subscribe(m_cell_observers[r][c]);
			}
		}
	}
	std::array<std::array<Cell, Col>, Row> m_cells{};
	std::array<std::array<CellObserver, Col>, Row> m_cell_observers{};
	[[no_unique_address]] OutputPolicy<Row, Col> m_op{};
};

#endif

