#ifndef CELL_CONSOLEOUTPUT_POLICY_H
#define CELL_CONSOLEOUTPUT_POLICY_H
#include "consoleoutput_policy.h"
#include "cell.h"

template <std::size_t Row, std::size_t Col>
using Cell2D = std::array<std::array<Cell, Col>, Row>;

template <std::size_t Row
		 , std::size_t Col>
class Cell_COP : public ConsoleOut<Cell2D<Row, Col>, Cell_COP<Row, Col>> {
public:
	void dprint(const Cell2D<Row, Col>& cells) const {
		print_impl(cells);
	}
private:
	void print_impl(const Cell2D<Row, Col>& cells) const {
		for(const auto& row: cells)
			for(const auto& cell : row)
				std::cout << (cell.get_state() == Cell::State::ALIVE ? '*' : '-') << (&row.back() == &cell ? '\n' : ' ');
		base_class::clear(Row, Col);
	}

	using base_class = ConsoleOut<Cell2D<Row, Col>, Cell_COP<Row, Col>>;
};

#endif

