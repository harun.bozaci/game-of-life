#ifndef CELL_OBSERVER_H
#define CELL_OBSERVER_H
#include <unordered_map>
#include <cstdint>
#include <memory>
#include "observer.h"

class Cell;

class CellObserver : public Observer<Cell> {
public:
	void state_changed(Cell& subject) override;
	int get_alive_neightbour_count() const;
private:
	int m_alive_neighbour_count{};	
};

#endif

