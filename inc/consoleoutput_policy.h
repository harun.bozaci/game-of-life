#ifndef CONSOLEOUTPUT_POLICY_H
#define CONSOLEOUTPUT_POLICY_H
#include "cell.h"
#include <array>
#include <thread>
#include <iostream>

template <typename T
		, typename Der>
class ConsoleOut {
public:
	void print(const T& elem) const {
		using std::literals::chrono_literals::operator""ms;
		std::this_thread::sleep_for(500ms);
		static_cast<const Der&>(*this).dprint(elem);
	}
protected:
	void clear(std::size_t Row, std::size_t Col) const {
		std::printf("\033[%luA", Row);
		std::printf("\033[%luD", Col * 2 + 1);
	}
	
};

#endif

