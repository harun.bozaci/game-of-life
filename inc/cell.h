#ifndef CELL_H
#define CELL_H
#include <cstdint>
#include "observable.h"

class Cell : public Observable<Cell> {
public:
	enum class State : std::uint8_t {
		DEAD,
		ALIVE
	};
	Cell(State state = State::DEAD);
	void set_next_state(State state);
	State get_state() const;
	void update_state();
private:
	State m_state;
	State m_next_state;
};

#endif

