#ifndef OBSERVER_H
#define OBSERVER_H

template <typename T>
class Observer {
public:
	virtual ~Observer() = default;
	virtual void state_changed(T& subject) = 0;
};

#endif

