#include "grid.h"
#include "cell_consoleoutput_policy.h" 
#include <random>

#ifdef ROW_CAP
constexpr std::size_t g_row_cap{ROW_CAP};
#else 
constexpr std::size_t g_row_cap{10};
#endif

#ifdef COL_CAP
constexpr std::size_t g_col_cap{COL_CAP};
#else 
constexpr std::size_t g_col_cap{10};
#endif


int main() {
	Grid<g_row_cap, g_row_cap, Cell_COP> grid;
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<std::size_t> idist(0, 9);
	for(int i = 0; i < 25; ++i) {
		grid.make_cell_alive(idist(gen), idist(gen));
	}
	while(true) {
		grid.display();
		grid.iterate();
	}
}

