#include "cell_observer.h"
#include "cell.h"

void CellObserver::state_changed(Cell& subject) {
	auto state = subject.get_state();
	switch(state) {
		using enum Cell::State;
		case DEAD:
			--m_alive_neighbour_count;
			break;
		case ALIVE:
			++m_alive_neighbour_count;
			break;
	}
}

int CellObserver::get_alive_neightbour_count() const {
	return m_alive_neighbour_count;
}

