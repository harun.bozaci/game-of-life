#include "cell.h"
#include "cell_observer.h"

Cell::Cell(Cell::State state) 
: m_state{state}
, m_next_state{state} {

}

void Cell::set_next_state(Cell::State state) {
	m_next_state = state;
}

void Cell::update_state() {
	if(m_state != m_next_state) {
		m_state = m_next_state;
		Observable::state_changed(*this);
	}
}

Cell::State Cell::get_state() const {
	return m_state;
}

