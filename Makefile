CXXFLAGS:= -Wall -Wshadow -Wextra -Werror -pedantic -std=c++2b
SRC_PATH:=./src
BIN_PATH:=./bin
OBJS_PATH:=./obj
OBJS:=$(subst ./src/,,$(patsubst %.cpp,%.o,$(wildcard ./src/*.cpp)))
INC:=-I./inc
BIN:=main
CXX:=g++-11

.PHONY:all
all: $(OBJS)
	$(CXX) $(CXXFLAGS) $(INC) -o "${BIN_PATH}/${BIN}" $(addprefix ${OBJS_PATH}/,${^})


%.o: $(SRC_PATH)/%.cpp
	$(CXX) $(CXXFLAGS) $(INC) -o "${OBJS_PATH}/${@}" -c "${<}"


.PHONY:clean
clean:
	rm -rf $(addprefix ${OBJS_PATH}/,${OBJS}) ${BIN_PATH}/${BIN}


.PHONY:run
run:
	${BIN_PATH}/${BIN}
